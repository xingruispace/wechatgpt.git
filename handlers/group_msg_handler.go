package handlers

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/eatmoreapple/openwechat"

	"github.com/869413421/wechatbot/gtp"
)

type limitConfig struct {
	ReplyText string `json:"reply_text"`
	Max       int    `json:"max"`
}

type limiter struct {
	lock sync.Mutex
	limitConfig
	Items         map[string]int
	LastWriteTime time.Time
}

func (l *limiter) Incr(key string) {
	l.lock.Lock()
	defer l.lock.Unlock()
	// 每天重置
	if l.LastWriteTime.IsZero() {
		l.LastWriteTime = time.Now()
		l.Items = make(map[string]int)
	}
	if time.Now().Day() != l.LastWriteTime.Day() {
		l.Items = make(map[string]int)
	}
	l.Items[key] = l.Items[key] + 1
	l.LastWriteTime = time.Now()
}

func (l *limiter) Get(key string) int {
	l.lock.Lock()
	defer l.lock.Unlock()
	return l.Items[key]
}

func (l *limiter) IsLimit(key string) bool {
	l.lock.Lock()
	defer l.lock.Unlock()
	fmt.Println(l, "limit")
	return l.Items[key] >= l.Max
}

var defaultLimiter *limiter

var logWriter io.Writer

type logEntry struct {
	UserName   string    `json:"username"`
	Request    string    `json:"request"`
	Reply      string    `json:"reply"`
	CreateTime time.Time `json:"create_time"`
}

var _ MessageHandlerInterface = (*GroupMessageHandler)(nil)

// GroupMessageHandler 群消息处理
type GroupMessageHandler struct {
}

// handle 处理消息
func (g *GroupMessageHandler) handle(msg *openwechat.Message) error {
	if msg.IsText() {
		return g.ReplyText(msg)
	}
	return nil
}

// NewGroupMessageHandler 创建群消息处理器
func NewGroupMessageHandler() MessageHandlerInterface {
	return &GroupMessageHandler{}
}

// ReplyText 发送文本消息到群
func (g *GroupMessageHandler) ReplyText(msg *openwechat.Message) error {
	// 接收群消息
	sender, err := msg.Sender()
	group := openwechat.Group{User: sender}
	log.Printf("Received Group %v Text Msg : %v", group.NickName, msg.Content)

	// 不是@的不处理
	if !msg.IsAt() {
		return nil
	}

	var logEntry logEntry
	defer func() {
		json.NewEncoder(logWriter).Encode(logEntry)
	}()

	// 获取@我的用户
	groupSender, err := msg.SenderInGroup()
	if err != nil {
		log.Printf("get sender in group error :%v \n", err)
		return err
	}

	logEntry.UserName = sender.NickName
	logEntry.Request = msg.Content
	logEntry.CreateTime = time.Now()

	if defaultLimiter.IsLimit(groupSender.UserName) {
		logEntry.Reply = defaultLimiter.ReplyText
		_, err = msg.ReplyText(defaultLimiter.ReplyText)
		if err != nil {
			log.Printf("response group error: %v \n", err)
		}
		return nil
	}
	defaultLimiter.Incr(sender.UserName)

	atText := "@" + groupSender.NickName + " "

	if UserService.ClearUserSessionContext(sender.ID(), msg.Content) {
		_, err = msg.ReplyText(atText + "上下文已经清空了，你可以问下一个问题啦。")
		if err != nil {
			log.Printf("response user error: %v \n", err)
		}
		return nil
	}

	// 替换掉@文本，设置会话上下文，然后向GPT发起请求。
	requestText := buildRequestText(sender, msg)
	if requestText == "" {
		return nil
	}
	reply, err := gtp.Completions(requestText)
	if err != nil {
		log.Printf("gtp request error: %v \n", err)
		_, err = msg.ReplyText("机器人神了，我一会发现了就去修。")
		if err != nil {
			log.Printf("response group error: %v \n", err)
		}
		return err
	}
	if reply == "" {
		return nil
	}

	// 回复@我的用户
	reply = strings.TrimSpace(reply)
	reply = strings.Trim(reply, "\n")
	// 设置上下文
	UserService.SetUserSessionContext(sender.ID(), requestText, reply)
	replyText := atText + reply
	logEntry.Reply = replyText
	_, err = msg.ReplyText(replyText)
	if err != nil {
		log.Printf("response group error: %v \n", err)
	}
	return err
}

// buildRequestText 构建请求GPT的文本，替换掉机器人名称，然后检查是否有上下文，如果有拼接上
func buildRequestText(sender *openwechat.User, msg *openwechat.Message) string {
	replaceText := "@" + sender.Self.NickName
	requestText := strings.TrimSpace(strings.ReplaceAll(msg.Content, replaceText, ""))
	if requestText == "" {
		return ""
	}
	requestText = UserService.GetUserSessionContext(sender.ID()) + requestText
	return requestText
}

func init() {
	{

		file, err := os.OpenFile("log.json", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			log.Fatal(err)
		}
		logWriter = file
	}

	{
		file, err := os.Open("cfg.json")
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()
		cfg := limitConfig{}
		if err = json.NewDecoder(file).Decode(&cfg); err != nil {
			log.Fatal(err)
		}
		defaultLimiter = &limiter{limitConfig: cfg}
	}

	fmt.Println(defaultLimiter)
}
