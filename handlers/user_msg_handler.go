package handlers

import (
	"encoding/json"
	"log"
	"strings"
	"time"

	"github.com/869413421/wechatbot/gtp"
	"github.com/eatmoreapple/openwechat"
)

var _ MessageHandlerInterface = (*UserMessageHandler)(nil)

// UserMessageHandler 私聊消息处理
type UserMessageHandler struct {
}

// handle 处理消息
func (g *UserMessageHandler) handle(msg *openwechat.Message) error {
	if msg.IsText() {
		return g.ReplyText(msg)
	}
	return nil
}

// NewUserMessageHandler 创建私聊处理器
func NewUserMessageHandler() MessageHandlerInterface {
	return &UserMessageHandler{}
}

// ReplyText 发送文本消息到群
func (g *UserMessageHandler) ReplyText(msg *openwechat.Message) error {
	// 接收私聊消息
	var logEntry logEntry
	defer func() {
		json.NewEncoder(logWriter).Encode(logEntry)
	}()

	sender, err := msg.Sender()
	logEntry.CreateTime = time.Now()
	logEntry.UserName = sender.NickName
	logEntry.Request = msg.Content
	log.Printf("Received User %v Text Msg : %v", sender.NickName, msg.Content)
	if defaultLimiter.IsLimit(sender.UserName) {
		logEntry.Reply = defaultLimiter.ReplyText
		_, err = msg.ReplyText(defaultLimiter.ReplyText)
		if err != nil {
			log.Printf("response user error: %v \n", err)
		}
		return nil
	}
	defaultLimiter.Incr(sender.UserName)
	if UserService.ClearUserSessionContext(sender.ID(), msg.Content) {
		logEntry.Reply = "上下文已经清空了，你可以问下一个问题啦。"
		_, err = msg.ReplyText("上下文已经清空了，你可以问下一个问题啦。")
		if err != nil {
			log.Printf("response user error: %v \n", err)
		}
		return nil
	}

	// 获取上下文，向GPT发起请求
	requestText := strings.TrimSpace(msg.Content)
	requestText = strings.Trim(msg.Content, "\n")

	requestText = UserService.GetUserSessionContext(sender.ID()) + requestText
	reply, err := gtp.Completions(requestText)
	if err != nil {
		log.Printf("gtp request error: %v \n", err)
		logEntry.Reply = "机器人神了，我一会发现了就去修。"
		msg.ReplyText("机器人神了，我一会发现了就去修。")
		return err
	}
	if reply == "" {
		return nil
	}

	// 设置上下文，回复用户
	reply = strings.TrimSpace(reply)
	reply = strings.Trim(reply, "\n")
	UserService.SetUserSessionContext(sender.ID(), requestText, reply)
	reply = reply + "\n\n本消息由chatGPT回复, 如需帮助请关注公众号星瑞元宇或者添加客服evolove007"
	logEntry.Reply = reply
	_, err = msg.ReplyText(reply)
	if err != nil {
		log.Printf("response user error: %v \n", err)
	}
	return err
}
